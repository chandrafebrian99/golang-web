FROM golang:alpine3.16
COPY . /app
EXPOSE 8080
CMD ["gunicorn", "-b", "0.0.0.0:8080","go", "run", "/app/main.go"]
